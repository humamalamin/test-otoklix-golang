package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"log"
	"otoklix-golang/handler"
	"otoklix-golang/post"
)

func main() {
	//dsn := "root:@tcp(127.0.0.1:3306)/test_otoklix?charset=utf8mb4&parseTime=True&loc=Local"
	//db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	db, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})

	if err != nil {
		log.Fatal(err.Error())
	}

	db.AutoMigrate(&post.Post{})

	postRepository := post.NewRepository(db)

	postService := post.NewService(postRepository)

	postHandler := handler.NewPostHandler(postService)

	router := gin.Default()
	router.GET("posts", postHandler.GetPosts)
	router.GET("posts/:id", postHandler.GetByID)
	router.POST("/posts", postHandler.Store)
	router.PUT("/posts/:id", postHandler.Update)
	router.DELETE("/posts/:id", postHandler.Delete)

	router.Run()

	fmt.Println("Connection is good")
}
