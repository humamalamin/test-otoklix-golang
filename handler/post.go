package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"otoklix-golang/helper"
	"otoklix-golang/post"
)

type postHandler struct {
	postService post.Service
}

func NewPostHandler(service post.Service) *postHandler {
	return &postHandler{service}
}

func (h *postHandler) GetPosts (c *gin.Context) {
	posts, err := h.postService.GetPosts()
	if err != nil {
		response := helper.APIResponse("Error To Get Content", http.StatusBadRequest, "error", nil)

		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.APIResponse("List Of Content", http.StatusOK, "success", post.FormatPosts(posts))
	c.JSON(http.StatusOK, response)
	return
}

func (h *postHandler) GetByID(c *gin.Context) {
	var input post.GetPostDetailInput

	err :=c.ShouldBindUri(&input)
	if err != nil {
		response := helper.APIResponse("Failed to get detail content", http.StatusBadRequest, "error", nil)

		c.JSON(http.StatusBadRequest, response)
		return
	}

	postDetail, err := h.postService.GetByID(input)
	formatter := post.FormatPost(postDetail)

	if err != nil {
		response := helper.APIResponse("Failed to get detail content", http.StatusBadRequest, "error", nil)

		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.APIResponse("content detail", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
	return
}

func (h *postHandler) Store(c *gin.Context) {
	var input post.StorePostInput

	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.APIResponse("Save content failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	newPost, err := h.postService.Store(input)
	if err != nil {
		response := helper.APIResponse(err.Error(), http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	formatter := post.FormatPost(newPost)

	response := helper.APIResponse("Save content success", http.StatusCreated, "success", formatter)

	c.JSONP(http.StatusCreated, response)
}

func (h *postHandler) Update(c *gin.Context) {
	var inputID post.GetPostDetailInput
	var input post.StorePostInput

	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.APIResponse("Update content failed", http.StatusBadRequest, "error", nil)

		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.APIResponse("Update content failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	updatePost, err := h.postService.Update(inputID, input)
	if err != nil {
		response := helper.APIResponse("Update content failed", http.StatusUnprocessableEntity, "error", nil)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	response := helper.APIResponse("Success update content", http.StatusOK, "success", post.FormatPost(updatePost))
	c.JSON(http.StatusOK, response)
	return
}

func (h *postHandler) Delete(c *gin.Context)  {
	var input post.GetPostDetailInput

	err :=c.ShouldBindUri(&input)
	if err != nil {
		response := helper.APIResponse("Failed to get content", http.StatusBadRequest, "error", nil)

		c.JSON(http.StatusBadRequest, response)
		return
	}

	postDetail, err := h.postService.Delete(input)
	formatter := post.FormatPost(postDetail)

	if err != nil {
		response := helper.APIResponse("Failed to get  content", http.StatusBadRequest, "error", nil)

		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.APIResponse("content detail", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
	return
}