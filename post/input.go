package post

type GetPostDetailInput struct {
	ID int `uri:"id" binding:"required"`
}

type StorePostInput struct {
	Title string `json:"title" binding:"required"`
	Content string `json:"content" binding:"required"`
}
