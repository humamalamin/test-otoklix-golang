package post

import (
	"gorm.io/gorm"
	"time"
)

type Post struct {
	gorm.Model
	ID int
	Title string
	Content string
	PublishedAt time.Time `gorm:"autoCreateTime"`
	CreatedAt time.Time
	UpdatedAt time.Time
}