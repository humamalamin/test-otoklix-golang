package post

import "time"

type PostFormatter struct {
	ID int `json:"id"`
	Title string `json:"title"`
	Content string `json:"content"`
	PublishedAt time.Time `json:"published_at"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func FormatPost(post Post) PostFormatter {
	postFormatter := PostFormatter{}
	postFormatter.ID = post.ID
	postFormatter.Title = post.Title
	postFormatter.Content = post.Content
	postFormatter.PublishedAt = post.PublishedAt
	postFormatter.CreatedAt = post.CreatedAt
	postFormatter.UpdatedAt = post.UpdatedAt

	return postFormatter
}

func FormatPosts(posts []Post) []PostFormatter {
	postsFormatter := []PostFormatter{}

	for _, post := range posts {
		postFormatter := FormatPost(post)
		postsFormatter = append(postsFormatter, postFormatter)
	}

	return postsFormatter
}