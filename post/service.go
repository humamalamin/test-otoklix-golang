package post

import "errors"

type Service interface {
	GetPosts()([]Post, error)
	GetByID(postID GetPostDetailInput)(Post, error)
	Store(input StorePostInput) (Post, error)
	Update(inputID GetPostDetailInput, input StorePostInput) (Post, error)
	Delete(postID GetPostDetailInput)(Post, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) GetPosts()([]Post, error) {
	posts, err := s.repository.FindAll()
	if err != nil {
		return posts, err
	}

	return posts, nil
}

func (s *service) GetByID (content GetPostDetailInput) (Post, error) {
	post, err :=s.repository.FindByID(content.ID)
	if err != nil {
		return post, err
	}

	if post.ID == 0 {
		return post, errors.New("No content on with that ID")
	}

	return post, nil
}

func (s *service) Store(input StorePostInput) (Post, error)  {
	post := Post{}
	post.Title = input.Title
	post.Content = input.Content

	newPost, err :=s.repository.Store(post)
	if err != nil {
		return newPost, err
	}

	return newPost, nil
}
func (s *service) Update(inputID GetPostDetailInput, input StorePostInput) (Post, error) {
	post, err := s.repository.FindByID(inputID.ID)
	if err != nil {
		return post, err
	}

	post.Title = input.Title
	post.Content = input.Content

	updatePost, err := s.repository.Update(post)
	if err != nil {
		return updatePost, err
	}

	return updatePost, nil
}

func (s *service) Delete(input GetPostDetailInput) (Post, error) {
	post, err := s.repository.Delete(input.ID)
	if err != nil {
		return post, err
	}

	return post, nil
}